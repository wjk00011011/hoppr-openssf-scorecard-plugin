## [0.0.1-dev.2](https://gitlab.com/hoppr/plugins/hoppr-openssf-scorecard-plugin/compare/v0.0.1-dev.1...v0.0.1-dev.2) (2023-04-03)


### Bug Fixes

* remove refs to `trio_typing` test dependency ([bee99e4](https://gitlab.com/hoppr/plugins/hoppr-openssf-scorecard-plugin/commit/bee99e45bacc26b38583f4cf211f53222aff829d))

## [0.0.1-dev.1](https://gitlab.com/hoppr/plugins/hoppr-openssf-scorecard-plugin/compare/v0.0.0...v0.0.1-dev.1) (2023-03-24)


### Bug Fixes

* improve Libraries.io best match logic ([eaf1d7b](https://gitlab.com/hoppr/plugins/hoppr-openssf-scorecard-plugin/commit/eaf1d7bb1ff962a3bc41b82cab3d6c7c5d1660a9))
* improved scorecard component properties ([4bb2a24](https://gitlab.com/hoppr/plugins/hoppr-openssf-scorecard-plugin/commit/4bb2a247d1a241d49986c3d232e5ec7f8bd417ed))
* Libraries.io response model types ([41ca418](https://gitlab.com/hoppr/plugins/hoppr-openssf-scorecard-plugin/commit/41ca4189ab01088d4cea31583dbd1d52ba501e51))
* plugin SBOM access ([32b6038](https://gitlab.com/hoppr/plugins/hoppr-openssf-scorecard-plugin/commit/32b6038ef011a36bd3d837f06a17e6c542b8d53a))

<!--next-version-placeholder-->
