"""
Test module for the hoppr_openssf_scorecard.models.libraries module
"""
from __future__ import annotations

from typing import Any

from hoppr_openssf_scorecard.models.libraries import LibrariesIOResponse


def test__getitem__():
    """
    Test LibrariesIOResponse.__getitem__ method
    """
    response_items: list[dict[str, Any]] = []
    names = ["test-pypi-pkg", "another-test"]

    # pylint: disable=duplicate-code
    for idx, name in enumerate(names):
        response_items.append(
            {
                "dependent_repos_count": 0,
                "dependents_count": 0,
                "deprecation_reason": None,
                "description": "",
                "forks": idx,
                "homepage": "",
                "keywords": [],
                "language": "",
                "latest_download_url": None,
                "latest_release_number": "",
                "latest_release_published_at": "",
                "latest_stable_release_number": "",
                "latest_stable_release_published_at": "",
                "license_normalized": False,
                "licenses": "",
                "name": name,
                "normalized_licenses": [],
                "package_manager_url": "",
                "platform": "",
                "rank": idx,
                "repository_license": "",
                "repository_status": None,
                "repository_url": f"https://github.com/hoppr/{name}",
                "stars": idx,
                "status": None,
                "versions": [],
            }
        )

    response = LibrariesIOResponse.parse_obj(response_items)

    assert response[0].repository_url == "https://github.com/hoppr/test-pypi-pkg"
    assert response[1].repository_url == "https://github.com/hoppr/another-test"
