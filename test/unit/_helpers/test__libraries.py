"""
Test module for the LibrariesIOScorecardHelper class
"""
from __future__ import annotations

import re

from typing import Callable

import httpx
import pytest

from hoppr import CredentialRequiredService, Credentials
from pytest import FixtureRequest, LogCaptureFixture, MonkeyPatch
from pytest_httpx import HTTPXMock

from hoppr_openssf_scorecard._helpers._libraries import LibrariesIOScorecardHelper


@pytest.fixture(scope="function", autouse=True)
def setup_fixture(monkeypatch: MonkeyPatch):
    """
    Pre-test fixture to automatically patch sleep methods
    """
    monkeypatch.setenv(name="LIBRARIES_API_KEY", value="TEST_LIBRARIES_API_KEY")
    monkeypatch.setattr(target="hoppr_openssf_scorecard._helpers._libraries.sleep", name=lambda secs: None)


@pytest.fixture(name="httpx_mock_response", scope="function")
def httpx_mock_response_fixture(request: FixtureRequest, httpx_mock: HTTPXMock) -> None:
    """
    Fixture to return mocked response from Libraries.io
    """
    names = getattr(request, "param", ["test-pypi-pkg", "another-test"])

    for idx, name in enumerate(names, start=1):
        httpx_mock.add_response(
            url=re.compile(pattern=f"^{LibrariesIOScorecardHelper.API_URL}/search.*page={idx}.*$"),
            status_code=200,
            json=[
                {
                    "dependent_repos_count": 0,
                    "dependents_count": 0,
                    "deprecation_reason": None,
                    "description": "",
                    "forks": idx,
                    "homepage": "",
                    "keywords": [],
                    "language": "",
                    "latest_download_url": None,
                    "latest_release_number": "",
                    "latest_release_published_at": "",
                    "latest_stable_release_number": "",
                    "latest_stable_release_published_at": "",
                    "license_normalized": False,
                    "licenses": "",
                    "name": name,
                    "normalized_licenses": [],
                    "package_manager_url": "",
                    "platform": "",
                    "rank": idx,
                    "repository_license": "",
                    "repository_status": None,
                    "repository_url": f"https://github.com/hoppr/{name}",
                    "stars": idx,
                    "status": None,
                    "versions": [],
                }
            ],
        )

    httpx_mock.add_response(
        url=re.compile(pattern=f"^{LibrariesIOScorecardHelper.API_URL}/search.*$"), status_code=200, json=[]
    )


@pytest.mark.usefixtures("httpx_mock_response", "assert_all_responses_were_requested")
@pytest.mark.parametrize(
    argnames=["httpx_mock_response", "purl_string", "expected_result", "assert_all_responses_were_requested"],
    argvalues=[
        (["no-match", "test-pypi"], "pkg:pypi/test-pypi@1.2.3", ["https://github.com/hoppr/test-pypi"], True),
        (["no-match", "test-mvn"], "pkg:maven/test-mvn@1.2.3", ["https://github.com/hoppr/test-mvn"], True),
        (["no-match", "test-go"], "pkg:golang/test-go@1.2.3", ["https://github.com/hoppr/test-go"], True),
        (["no-match", "test-npm"], "pkg:npm/test-npm@1.2.3", ["https://github.com/hoppr/test-npm"], True),
        (["no-match", "test-npm"], "pkg:npm/test-npm@1.2.3", ["https://github.com/hoppr/test-npm"], True),
        (["no-match", "test-rpm"], "pkg:rpm/test-rpm@1.2.3", ["https://github.com/hoppr/test-rpm"], True),
        (["test-rpm", "test-rpm"], "pkg:rpm/test-rpm@1.2.3", [], False),
        (["no-match-1", "no-match-2"], "pkg:pypi/test-pypi@1.2.3", [], True),
        ([], "pkg:pypi/test-pypi@1.2.3", [], True),
    ],
    indirect=["httpx_mock_response"],
)
async def test_get_vcs_repo_url(helper: LibrariesIOScorecardHelper, purl_string: str, expected_result: list[str]):
    """
    Test LibrariesIOScorecardHelper.get_vcs_repo_url method
    """
    result = await helper.get_vcs_repo_url(purl_string)
    assert result == expected_result


@pytest.mark.usefixtures("httpx_mock_response", "assert_all_responses_were_requested")
@pytest.mark.parametrize(
    argnames=["httpx_mock_response", "assert_all_responses_were_requested"],
    argvalues=[
        (
            ["test-1", "test-2", "test-3", "test-4", "test-5", "test-6", "test-7", "test-8", "test-9", "test-10"],
            False,
        )
    ],
    indirect=["httpx_mock_response"],
)
async def test_get_vcs_repo_url_bad_relevance_score(helper: LibrariesIOScorecardHelper, caplog: LogCaptureFixture):
    """
    Test LibrariesIOScorecardHelper.get_vcs_repo_url method with score exceeding relevance threshold
    """
    # Without an exact match, score should be calculated to exceed relevance threshold
    result = await helper.get_vcs_repo_url("pkg:pypi/test@1.2.3")

    assert result == []
    assert caplog.messages[-1] == "Best match did not meet relevance requirement"


async def test_get_vcs_repo_url_no_api_key(helper: LibrariesIOScorecardHelper, monkeypatch: MonkeyPatch):
    """
    Test LibrariesIOScorecardHelper.get_vcs_repo_url method raises EnvironmentError
    """
    monkeypatch.delenv(name="LIBRARIES_API_KEY", raising=False)

    with pytest.raises(
        expected_exception=EnvironmentError,
        match=(
            "Either a credentials file with an entry for 'https://libraries.io/api' "
            "or the environment variable LIBRARIES_API_KEY must be set to use this plugin."
        ),
    ):
        await helper.get_vcs_repo_url("pkg:pypi/test-pypi-pkg@1.2.3")


async def test_get_vcs_repo_url_not_found_error(helper: LibrariesIOScorecardHelper, httpx_mock: HTTPXMock):
    """
    Test LibrariesIOScorecardHelper.get_vcs_repo_url method raises httpx.HTTPStatusError on 404 response
    """
    httpx_mock.add_response(url=re.compile(pattern=f"^{helper.API_URL}/search.*$"), status_code=404)

    with pytest.raises(expected_exception=httpx.HTTPStatusError):
        result = await helper.get_vcs_repo_url("pkg:pypi/test-pypi-pkg@1.2.3")
        assert result is None


async def test_get_vcs_repo_url_timeout(helper: LibrariesIOScorecardHelper, httpx_mock: HTTPXMock):
    """
    Test LibrariesIOScorecardHelper.get_vcs_repo_url method returns None if timeout occurs
    """
    httpx_mock.add_exception(
        url=re.compile(pattern=f"^{helper.API_URL}/search.*$"),
        exception=httpx.TimeoutException(message="TIMEOUT"),
    )

    with pytest.raises(expected_exception=httpx.TimeoutException):
        result = await helper.get_vcs_repo_url("pkg:pypi/test-pypi-pkg@1.2.3")
        assert result is None


@pytest.mark.usefixtures("httpx_mock_response", "assert_all_responses_were_requested")
@pytest.mark.parametrize(
    argnames=["cred_object", "assert_all_responses_were_requested"],
    argvalues=[({"url": LibrariesIOScorecardHelper.API_URL}, False)],
    indirect=["cred_object"],
)
async def test_get_vcs_repo_url_with_credentials(
    helper: LibrariesIOScorecardHelper,
    find_credentials: Callable[[str], CredentialRequiredService],
    monkeypatch: MonkeyPatch,
):
    """
    Test LibrariesIOScorecardHelper.get_vcs_repo_url method with parsed Credentials input
    """
    monkeypatch.setattr(target=Credentials, name="find", value=find_credentials)

    result = await helper.get_vcs_repo_url("pkg:pypi/test-pypi-pkg@1.2.3")
    assert result == ["https://github.com/hoppr/test-pypi-pkg"]
