"""
Hoppr OpenSSF Scorecard plugin integration tests
"""
from __future__ import annotations

import json
import sys
import tarfile

from pathlib import Path
from typing import Any

import jmespath
import requests

from hoppr_openssf_scorecard.models.scorecard import ScorecardResponse

COLUMN_WIDTH = 46

# Mapping of delivered SBOM property names to JMESPath expressions used to search the associated Scorecard response data
PROPERTY_MAP = {
    "hoppr:scorecard:date": "date",
    "hoppr:scorecard:repo:name": "repo.name",
    "hoppr:scorecard:repo:commit": "repo.commit",
    "hoppr:scorecard:scorecard:version": "scorecard.version",
    "hoppr:scorecard:scorecard:commit": "scorecard.commit",
    "hoppr:scorecard:score": "score",
    "hoppr:scorecard:check:Binary-Artifacts": "checks[? name == 'Binary-Artifacts'] | [0].score",
    "hoppr:scorecard:check:Branch-Protection": "checks[? name == 'Branch-Protection'] | [0].score",
    "hoppr:scorecard:check:CI-Tests": "checks[? name == 'CI-Tests'] | [0].score",
    "hoppr:scorecard:check:CII-Best-Practices": "checks[? name == 'CII-Best-Practices'] | [0].score",
    "hoppr:scorecard:check:Code-Review": "checks[? name == 'Code-Review'] | [0].score",
    "hoppr:scorecard:check:Contributors": "checks[? name == 'Contributors'] | [0].score",
    "hoppr:scorecard:check:Dangerous-Workflow": "checks[? name == 'Dangerous-Workflow'] | [0].score",
    "hoppr:scorecard:check:Dependency-Update-Tool": "checks[? name == 'Dependency-Update-Tool'] | [0].score",
    "hoppr:scorecard:check:Fuzzing": "checks[? name == 'Fuzzing'] | [0].score",
    "hoppr:scorecard:check:License": "checks[? name == 'License'] | [0].score",
    "hoppr:scorecard:check:Maintained": "checks[? name == 'Maintained'] | [0].score",
    "hoppr:scorecard:check:Packaging": "checks[? name == 'Packaging'] | [0].score",
    "hoppr:scorecard:check:Pinned-Dependencies": "checks[? name == 'Pinned-Dependencies'] | [0].score",
    "hoppr:scorecard:check:SAST": "checks[? name == 'SAST'] | [0].score",
    "hoppr:scorecard:check:Security-Policy": "checks[? name == 'Security-Policy'] | [0].score",
    "hoppr:scorecard:check:Signed-Releases": "checks[? name == 'Signed-Releases'] | [0].score",
    "hoppr:scorecard:check:Token-Permissions": "checks[? name == 'Token-Permissions'] | [0].score",
    "hoppr:scorecard:check:Vulnerabilities": "checks[? name == 'Vulnerabilities'] | [0].score",
    "hoppr:scorecard:check:Webhooks": "checks[? name == 'Webhooks'] | [0].score",
}


def check_result(purl_string: str, delivered_sbom: dict[str, Any], repo_url: str) -> list[str]:
    """
    Check component properties in delivered SBOM against direct response from Scorecard API

    Args:
        purl_string (str): The PURL of the component
        delivered_sbom (dict[str, Any]): The dict representation of the SBOM modified by the plugin
        repo_url (str): Pre-determined VCS repo URL of component package

    Returns:
        list[str]: Property checks that failed
    """
    repo_url = f"https://api.securityscorecards.dev/projects/{repo_url}"
    response = requests.get(url=repo_url, timeout=30)
    response.raise_for_status()
    scorecard_response = ScorecardResponse(**response.json())

    failed: list[str] = []

    # Print "headers" of results table
    print("PROPERTY".ljust(COLUMN_WIDTH), "VALUE".ljust(COLUMN_WIDTH), "EXPECTED".ljust(COLUMN_WIDTH))
    print("".ljust(COLUMN_WIDTH, "-"), "".ljust(COLUMN_WIDTH, "-"), "".ljust(COLUMN_WIDTH, "-"))

    for property_name, scorecard_search in PROPERTY_MAP.items():
        property_search = " ".join(
            [
                f"components[? purl == '{purl_string}'] |",
                f"[0].properties[? name == '{property_name}'] |",
                "[0].value",
            ]
        )

        property_value = str(jmespath.search(expression=property_search, data=delivered_sbom))
        scorecard_value = str(jmespath.search(expression=scorecard_search, data=scorecard_response.dict()))

        print(
            property_name.ljust(COLUMN_WIDTH),
            property_value.ljust(COLUMN_WIDTH),
            scorecard_value.ljust(COLUMN_WIDTH),
        )

        if str(property_value) != str(scorecard_value):
            failed.append(property_name)

    return failed


def main():
    """
    Entrypoint for integration test
    """
    test_dir = Path(__file__).parent / sys.argv[1]

    test_data: list[dict[str, Any]] = json.loads((test_dir / "test-data.json").read_text())

    # Extract delivered SBOM from tar bundle
    with tarfile.open(name=test_dir / "tarfile.tar.gz", mode="r:gz") as tar:
        extracted = tar.extractfile(member="./generic/_metadata_/_delivered_bom.json")
        if extracted is None:
            raise IOError("Delivered SBOM file not found in tar bundle.")

        delivered_sbom = json.load(extracted)

    failed = False
    failed_checks: dict[str, list[str]] = {}

    # Submit request against Scorecard API with pre-determined repo URL for comparison
    for data in test_data:
        print(f"\nChecking properties for component '{data['purl']}':\n")

        failed_checks[data["purl"]] = check_result(
            purl_string=data["purl"], delivered_sbom=delivered_sbom, repo_url=data["repo_url"]
        )

        if len(failed_checks[data["purl"]]) > 0:
            failed = True

    # Print summary
    if failed:
        for purl_string, failures in failed_checks.items():
            if len(failures) > 0:
                print(f"Failed checks for '{purl_string}':")
                print("\n".join([f"    {failure}" for failure in failures]))

        sys.exit(1)
    else:
        print("\nAll checks passed!\n")
